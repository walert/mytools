package com.qingquan.web;

import com.qingquan.common.exception.MyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by myworld1215 on 2017/2/6.
 */
@Controller
@RequestMapping(value = "hello")
public class HelloController extends BaseController {

    @RequestMapping(value = "say")
    @ResponseBody
    public String sayHelloWorld() throws MyException{
        throw new MyException("好像出问题了");
    }

    @RequestMapping("index")
    public String index(ModelMap map) throws MyException {
        map.addAttribute("host", "魏清泉");
        throw new MyException("好像又出问题了");
        //return "index";
    }

    @RequestMapping(value = "text")
    @ResponseBody
    public String returnStringForPage(){
        StringBuilder sb = new StringBuilder();
        return sb.toString();
    }
}
