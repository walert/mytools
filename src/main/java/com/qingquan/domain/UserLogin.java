package com.qingquan.domain;

/**
 * Created by myworld1215 on 2017/2/9.
 */
public class UserLogin {

    private String userLoginId;
    private String description;
    private String password;

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
