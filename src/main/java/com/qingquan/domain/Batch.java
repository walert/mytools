package com.qingquan.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by myworld1215 on 2017/2/9.
 */
public class Batch implements Serializable {

    private static final long serialVersionUID = 4486666447344188894L;

    private Long id;
    private Date batchTime;
    private String batchNo;
    private Long expressCompany;
    private Long wareId;
    private char status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBatchTime() {
        return batchTime;
    }

    public void setBatchTime(Date batchTime) {
        this.batchTime = batchTime;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public Long getExpressCompany() {
        return expressCompany;
    }

    public void setExpressCompany(Long expressCompany) {
        this.expressCompany = expressCompany;
    }

    public Long getWareId() {
        return wareId;
    }

    public void setWareId(Long wareId) {
        this.wareId = wareId;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }
}