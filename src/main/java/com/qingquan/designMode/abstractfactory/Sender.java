package com.qingquan.designMode.abstractfactory;

public interface Sender {
	public void Send();
}
