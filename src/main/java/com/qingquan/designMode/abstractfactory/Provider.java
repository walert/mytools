package com.qingquan.designMode.abstractfactory;

public interface Provider {
	public Sender produce();
}
