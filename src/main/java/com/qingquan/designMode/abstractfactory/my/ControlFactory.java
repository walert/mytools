package com.qingquan.designMode.abstractfactory.my;

/**
 * Created by myworld1215 on 2017/2/16.
 */
public interface ControlFactory {
        TableControl createControl();
}
