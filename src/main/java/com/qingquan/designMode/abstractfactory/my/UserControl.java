package com.qingquan.designMode.abstractfactory.my;

/**
 * Created by myworld1215 on 2017/2/16.
 */
public class UserControl implements TableControl {
    @Override
    public void insert() {
        System.out.println("this user insert !!!");
    }

    @Override
    public void update() {
        System.out.println("this user update !!!");
    }

    @Override
    public void delate() {
        System.out.println("this user delete !!!");
    }

    @Override
    public void select() {
        System.out.println("this user select !!!");
    }
}
