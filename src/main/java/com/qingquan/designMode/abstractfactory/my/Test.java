package com.qingquan.designMode.abstractfactory.my;

/**
 * Created by myworld1215 on 2017/2/16.
 */
public class Test {
    public static void main(String[] args) {

        ControlFactory factory = new CustomerFactory();
        TableControl control = factory.createControl();
        control.insert();
        control.delate();
        control.select();
        control.update();


        factory = new UserFactory();
        control = factory.createControl();
        control.insert();
        control.delate();
        control.select();
        control.update();


    }
}
