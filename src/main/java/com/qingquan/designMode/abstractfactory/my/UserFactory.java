package com.qingquan.designMode.abstractfactory.my;

/**
 * Created by myworld1215 on 2017/2/16.
 */
public class UserFactory implements ControlFactory {
    @Override
    public TableControl createControl() {
        return new UserControl();
    }
}
