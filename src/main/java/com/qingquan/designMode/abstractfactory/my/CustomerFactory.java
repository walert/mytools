package com.qingquan.designMode.abstractfactory.my;

/**
 * Created by myworld1215 on 2017/2/16.
 */
public class CustomerFactory implements ControlFactory {
    @Override
    public TableControl createControl() {
        return new CustomerControl();
    }
}
