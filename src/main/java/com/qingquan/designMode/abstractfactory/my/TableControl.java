package com.qingquan.designMode.abstractfactory.my;

/**
 * Created by myworld1215 on 2017/2/16.
 */
public interface TableControl {
    void insert();
    void update();
    void delate();
    void select();
}
