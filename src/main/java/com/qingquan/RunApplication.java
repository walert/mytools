package com.qingquan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by myworld1215 on 2017/1/24.
 */
@SpringBootApplication
public class RunApplication {
//    public class RunApplication extends SpringBootServletInitializer {
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(RunApplication.class);
//    }

    public static void main(String[] args) {
        SpringApplication.run(RunApplication.class,args);
    }
}
