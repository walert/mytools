package com.qingquan.common.exception;

/**
 * Created by myworld1215 on 2017/2/13.
 */
public class MyException extends Exception {

    public MyException(String message) {
        super(message);
    }

}