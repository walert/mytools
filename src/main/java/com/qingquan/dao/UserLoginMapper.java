package com.qingquan.dao;

import com.qingquan.domain.UserLogin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Created by myworld1215 on 2017/2/9.
 */
@Mapper
public interface UserLoginMapper extends BaseMapper<UserLogin> {

    @Select("select * from UserLogin where userLoginId = ${id}")
    UserLogin findById(@Param("id") String id);
}
