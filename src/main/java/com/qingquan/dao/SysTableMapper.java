package com.qingquan.dao;

import com.qingquan.domain.model.SysTable;
import com.qingquan.domain.model.SysTableExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysTableMapper {
    int countByExample(SysTableExample example);

    int deleteByExample(SysTableExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysTable record);

    int insertSelective(SysTable record);

    List<SysTable> selectByExample(SysTableExample example);

    SysTable selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysTable record, @Param("example") SysTableExample example);

    int updateByExample(@Param("record") SysTable record, @Param("example") SysTableExample example);

    int updateByPrimaryKeySelective(SysTable record);

    int updateByPrimaryKey(SysTable record);
}