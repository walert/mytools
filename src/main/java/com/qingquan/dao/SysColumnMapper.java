package com.qingquan.dao;

import com.qingquan.domain.model.SysColumn;
import com.qingquan.domain.model.SysColumnExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysColumnMapper {
    int countByExample(SysColumnExample example);

    int deleteByExample(SysColumnExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysColumn record);

    int insertSelective(SysColumn record);

    List<SysColumn> selectByExample(SysColumnExample example);

    SysColumn selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysColumn record, @Param("example") SysColumnExample example);

    int updateByExample(@Param("record") SysColumn record, @Param("example") SysColumnExample example);

    int updateByPrimaryKeySelective(SysColumn record);

    int updateByPrimaryKey(SysColumn record);
}