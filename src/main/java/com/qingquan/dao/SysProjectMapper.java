package com.qingquan.dao;

import com.qingquan.domain.model.SysProject;
import com.qingquan.domain.model.SysProjectExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysProjectMapper {
    int countByExample(SysProjectExample example);

    int deleteByExample(SysProjectExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SysProject record);

    int insertSelective(SysProject record);

    List<SysProject> selectByExample(SysProjectExample example);

    SysProject selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SysProject record, @Param("example") SysProjectExample example);

    int updateByExample(@Param("record") SysProject record, @Param("example") SysProjectExample example);

    int updateByPrimaryKeySelective(SysProject record);

    int updateByPrimaryKey(SysProject record);
}